const express = require('express')
const bodyParser = require('body-parser')

const app = express()
const log = console.log
const port = process.env.PORT || 3000

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.get('/', (req, res) => {
  const someData = {
    message: 'hello world',
  }
  res.status(200).send(someData)
})

app.get('/another', (req, res) => {
  const { query } = req
  log(query)
  res.status(200).send({ query })
})

app.post('/postme', (req, res) => {
  const { body } = req
  log(body)
  res.status(200).send({ body })
})

app.listen(port, (err) => {
  console.log(`
🚀  Server Start 🚀
🚀  Server runs on http://localhost:${port}`)
})
